# Wiremind data science test

This repository contains the files for the data science test of Wiremind.

+ `data_exploration.ipynb` performs a preliminary exploration of the data set `ds_train.csv`.
+ `model.ipynb` contains the pipeline that is used for the regression problem as well as some post-processing of the results.
+ `post_processing.py` contains functions for post-processing tasks.

