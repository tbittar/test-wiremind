''' Script to do some post-processing of the predicted demand'''

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def round_prediction(y_pred):
    '''As we try to predict positive integer variables a simple rounding greatly improves the mean absolute error and the median absolute error. Negative predictions are set to 0

    y_pred: np.array, predicted value of the target
    '''

    y_pred[y_pred < 0] = 0
    y_pred = np.around(y_pred)
    return y_pred

def eval_model(y_true, y_pred, metrics_list):
    '''Evaluate the model using different metrics

    y_true: np.array, true value of the target

    y_pred: np.array, predicted value of the target

    metrics_list: list of sklearn.metrics functions, metrics to compute
    '''

    for metrics in metrics_list:
        try: # There are some errors when using the mean squared log error on negative data
            print('{}: {}'.format(metrics.__name__, metrics(y_true, y_pred)))
        except:
            pass

def demand_distrib(y_true, y_pred):
    ''' Plot the distribution of the true and the predicted demand
    
    y_true: np.array, true value of the target

    y_pred: np.array, predicted value of the target'''

    fig, ax = plt.subplots(1, 2, figsize=(16, 8))

    bins = range(int(max(y_true.max(), y_pred.max()))+1)
    
    sns.distplot(y_true, bins, kde=False, ax=ax[0])
    sns.distplot(y_pred, bins, kde=False, ax=ax[1])

    ax[0].set_yscale('log')
    ax[1].set_yscale('log')

    ax[0].set_ylim([0,55000])
    ax[1].set_ylim([0,55000])
    
    ax[0].set_xlabel('demand')
    ax[1].set_xlabel('demand')
    
    ax[0].set_ylabel('y_test')
    ax[1].set_ylabel('y_pred')

    ax[0].set_title('Distribution of the true demand')
    ax[1].set_title('Distribution of the predicted demand')

def error_distrib(y_true, y_pred):
    ''' Plot the distribution of the error between the true and the predicted demand
    
    y_true: np.array, true value of the target

    y_pred: np.array, predicted value of the target'''

    fig, ax = plt.subplots()
    sns.distplot(y_pred - y_true, kde=False, ax=ax)
    ax.set_yscale('log')
    ax.set_ylabel('log(y_pred - y_true)')
    ax.set_title('Distribution of the error on the demand')